Complete the table below with your results, and then provide your interpretation at the end.

This version is for using remote access such as via the web interface at feng-linux.leeds.ac.uk.


Note that:

- See Lecture 8 and the start of Worksheet 2 for instructions on how to compile and launch an MPI.

- Use MPICH rather than OpenMPI.

- When calculating the parallel speed-up S, use the time output by the code, which corresponds
  to the parallel calculation and does not include reading in the file or performing the serial check.

- Take as the serial execution time the time output by the code when run with a single process
  (hence the speed-up for 1 process must be 1.0, as already filled in the table).

- If you used a different input.txt to test your code, ensure you restore the original input.txt for
  the timing runs.


No. Process:                        Mean time (average of 3 runs)           Parallel speed-up, S:
===========                         ============================:           ====================
1                                           0.0198167 s                             1.0
2                                           0.0113063 s                             1.75
4                                           0.00687933 s                            2.88
8                                           0.00574207 s                            3.45
16                                          0.06965483 s                            0.28
32                                          0.2011837 s                             0.099
64                                          0.3275647 s                             0.060
128                                         1.0125323 s                             0.020

Architecture that the timing runs were performed on: feng-linux.leeds.ac.uk


A brief interpretation of these results (2-3 sentences should be enough):
The parallel speedup initially increases quite significantly as the number of processes doubles
since the computation is faster when carried out in parallel across multiple processes but then
decreases sharply from 16 processes onwards due to parallel overheads. As the number of processes
continues to double, parallel overheads such as communication between the processes, time and resources
to create, schedule and destroy the processes and computation to distribute the problem size causes
the parallel execution time to be much slower than in serial.
