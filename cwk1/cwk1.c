//
// Starting code for coursework 1. Requires "cwk1_extra.h", which should be placed in the same directory.
//
// Compile with (on the school machines) or use the provided makefile:
// gcc -fopenmp -Wall -o cwk1 cwk1.c
// where '-fopenmp' is redundant until OpenMP functionality has been added.
//


//
// Standard includes.
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>


//
// The set itself and the routines to initialise, destroy and print it are defined
// in the file cwk1_extra.h that you should also have downloaded. Although you are
// free to inspect this file, fo not alter the routines it contains, or replace them
// with your own versions, as they will be replaced as part of the assessment.
//
// cwk1_extra.h includes the following global variables:
//
// int setSize;                 // The current size of the set = number of items it contant
// int maxSetSize;              // The maximum size for the set (which should not be altered).
// int *set;                    // The set itself, an array of size maxSetSize.
//
// It also contains the following routines:
//
// int initSet( int maxSize );  // Initialises the set to the given maximum size.
//                              // Returns 0 if successful, -1 for an allocation error.
//
// void destroySet();           // Destroys the set (i.e. deletes all associated resources).
//
// void printSet();             // Prints the set.
//
#include "cwk1_extra.h"


//
// Add a value to the set if it does not currently exist.
//
void addToSet( int value )
{
    // flag to indicate whether to add value to set
    int i, flag = 0;

    #pragma omp critical
    {
      // Cannot exceed the maximum size.
      if( setSize==maxSetSize ) flag = 1;

      // Since sets should not have duplicates, first check this value is not already in the set.
      for( i=0; i<setSize; i++ )
          if( set[i]==value )
              flag = 1;

      if (flag == 0) {
        // Only reach this point if the value was not found and there is room to add to the set.
        set[setSize] = value;
        setSize++;
      }
    }
}


//
// Remove a value from the set, if it exists, and shuffle the remaining values so the set remains contiguous.
//
void removeFromSet( int value )
{
    int i;

    // Find where the index in the set corresponding to the value, if any.
    int index = -1;
    #pragma omp parallel for
    for( i=0; i<setSize; i++ )
        if( set[i]==value )
            index = i;

    // If found, 'remove'. Here, 'removal' is achieved by moving all values later in the set down by one index,
    // and also reducing the set size by one.
    if( index!= -1 )
    {
        // copy set into a temporary set
        int tempSet[setSize];
        #pragma omp parallel for
        for (i = 0; i < setSize; i++) {
          tempSet[i] = set[i];
        }

        #pragma omp parallel for
        for( i=index; i<setSize-1; i++ )
            set[i] = tempSet[i+1];
        setSize--;
    }
}


//
// Parallel sort.
//
void sortSet()
{
    int redBlack, i, j;

    for (i = 0; i < setSize-1; i++) {
      // sort odd and even parts separately
      for (redBlack = 0; redBlack < 2; redBlack++) {

        #pragma omp parallel for
        for (j = 0; j < setSize-1; j++) {
          if (j%2 == redBlack) {
            // swap values if first one is greater
            if (set[j] > set[j+1]) {
              int temp = set[j];
              set[j] = set[j+1];
              set[j+1] = temp;
            }
          }
        }
      }
    }

}

//
// Main.
//
int main( int argc, char **argv )
{
    int i;

    // Get from the command line the maximum set size, the number of values to add, and the number to remove.
    // You do not need to alter this section of the code.
    if( argc!=4 )
    {
        printf( "Need 3 command line arguments: The maximum set size, the no. values to add, and the no. values to remove.\n" );
        return EXIT_FAILURE;
    }

    int
        maxSetSize  = atoi(argv[1]),
        numToAdd    = atoi(argv[2]),
        numToRemove = atoi(argv[3]);

    if( maxSetSize<=0 || numToAdd<0 || numToRemove<0 )
    {
        printf( "Invalid arguments: max. size must be > 0, and the numbers to add and remove must both be non-negative." );
        return EXIT_FAILURE;
    }

    // Initialise the set. Returns -1 if could not allocate memory.
    if( initSet(maxSetSize)==-1 ) return EXIT_FAILURE;

    // Seed the psuedo-random number generator to the current time.
    srand( time(NULL) );

    // Add random numbers in the range 0 to maxSetSize-1 inclusive. You are asked to parallelise this loop in Task 1.
    #pragma omp parallel for
    for( i=0; i<numToAdd; i++ )
        addToSet( rand()%maxSetSize );

    printf( "Attempted to add %i random values. Current state of set:\n", numToAdd );
    printSet();

    // Remove values from the set; random values from the same range as they were added.
    for( i=0; i<numToRemove; i++ )
        removeFromSet( rand()%maxSetSize );

    printf( "\nRemoved up to %i random values if present. Current state of set:\n", numToRemove );
    printSet();

    // Finally, sort the set in increasing order.
    sortSet();
    printf( "\nCalled sortSet(). Current state of set:\n" );
    printSet();

    // You MUST call this function just before finishing - do NOT remove, or change the definition of destroySet(),
    // as it will be changed with a different version for assessment.
    destroySet();

    return EXIT_SUCCESS;
}
